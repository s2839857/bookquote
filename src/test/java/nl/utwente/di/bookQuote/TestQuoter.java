package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
// ∗ ∗ ∗ Tests the Quoter ∗ //
public class TestQuoter {
    @Test
    public void testBook1 ( ) throws Exception {
        Quoter quoter = new Quoter();
        double fahrenheit = quoter.getFahrenheit("25") ;
        Assertions.assertEquals(77.0,fahrenheit,0.0,"Temperature in Fahrenheit");
    }
}