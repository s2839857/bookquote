package nl.utwente.di.bookQuote;

public class Quoter {
    double getFahrenheit(String celsius){
        double fahrenheit = Integer.parseInt(celsius) * 9 / 5 + 32;

        return fahrenheit;
    }

}
